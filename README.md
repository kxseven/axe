<p align="center">
  <a href="https://github.com/kxseven/axe">
  <img src="https://rawgit.com/kxseven/axe/master/docs/media/images/logo.png">
  </a>
</p>

# AXE: Cloud Hatchet

[![GitHub license](https://img.shields.io/github/license/kxseven/axe.svg?style=flat-square)](https://github.com/kxseven/axe/blob/master/LICENSE)
[![GitHub release](https://img.shields.io/github/release/kxseven/axe.svg?style=flat-square)](https://github.com/kxseven/axe/releases)
[![GitHub changelog](https://img.shields.io/badge/docs-CHANGELOG-orange.svg?style=flat-square)](http://awsaxe.readthedocs.io/en/latest/content/about/release-notes/)

## What is AXE and how can it help me?

The AXE tools are a collection of Python and BASH helper scripts that are intended to augment your existing interaction with AWS by;

- Helping with the loading of AWS credentials into the environment that can be re-used by all of your existing AWS toolset; Terraform, AWS CLI, Terraforming, Ansible, etc
- Helping to generate useful information about existing resources you already have on AWS in a format that can be used as part of a pipeline for other tools/apps

---

 - View the [AXE Documentation]

[AXE Documentation]: http://awsaxe.readthedocs.io/


